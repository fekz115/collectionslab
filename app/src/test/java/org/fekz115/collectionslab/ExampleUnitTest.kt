package org.fekz115.collectionslab

import org.fekz115.collectionslab.model.TaskCollection
import org.junit.Test

class ExampleUnitTest {

    @Test
    fun freshMeat(){
        val testedCollection = TaskCollection<Int, Any>()
        val inArray = arrayOf(8, 10, 14, 3, 6, 1, 7, 13, 4)
        for(i in inArray){
            testedCollection[i] = 'A'
        }
        printCollection(testedCollection)
        var t = testedCollection.remove(10)
        printCollection(testedCollection)
        assert(t == 'A')
        t = testedCollection.remove(8)
        printCollection(testedCollection)
        assert(t == 'A')
        t = testedCollection.remove(1)
        printCollection(testedCollection)
        assert(t == 'A')
        t = testedCollection.remove(14)
        printCollection(testedCollection)
        assert(t == 'A')
        t = testedCollection.remove(0)
        printCollection(testedCollection)
        assert(t == null)
    }

    fun printCollection(testedCollection : TaskCollection<Int, Any>){
        for(i in testedCollection){
            print("${i.key} ")
        }
        println()
    }

}
