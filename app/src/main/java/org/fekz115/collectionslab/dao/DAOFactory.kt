package org.fekz115.collectionslab.dao

import android.content.Context
import org.fekz115.collectionslab.dao.sqlitedao.SQLiteDAOFactory
import org.fekz115.collectionslab.model.DataObjectType
import java.io.Serializable

abstract class DAOFactory(val context : Context) {

    abstract fun onExit()

    abstract fun getTables() : Array<Pair<DataObjectType, DAO>>
    abstract fun createTable(table : DataObjectType) : DAO
    abstract fun deleteTable(table : DataObjectType)
}

enum class Factories : Serializable {

    sqlite{
        override fun getFactory(context: Context) : DAOFactory {
            return SQLiteDAOFactory(context)
        }
    };

    abstract fun getFactory(context: Context) : DAOFactory

}