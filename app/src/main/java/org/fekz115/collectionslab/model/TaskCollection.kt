package org .fekz115.collectionslab.model

import java.lang.Exception
import java.util.*

class TaskCollection<K : Comparable<K>, V> : MutableMap<K, V>{

    data class TaskMapEntry<K : Comparable<K>, V>(override val key : K, override var value : V) : MutableMap.MutableEntry<K, V>, Comparable<TaskMapEntry<K, V>>{

        override fun compareTo(other: TaskMapEntry<K, V>): Int {
            return key.compareTo(other.key)
        }

        override fun setValue(newValue: V): V {
            value = newValue
            return value
        }

    }

    inner class TaskCollectionIterator : Iterator<TaskMapEntry<K, V>>{

        private var temp = 0
        private var indicesArray = arrayOfNulls<Int>(length)

        init{
            go(0)
            temp = 0
        }

        private fun go(i : Int){
            if((i < tree.size))
                if((tree[i] != null)) {
                    go(i * 2 + 1)
                    indicesArray[temp++] = i
                    go(i * 2 + 2)
                }
        }

        override fun hasNext(): Boolean {
            return temp < length
        }

        override fun next(): TaskMapEntry<K, V> {
            return tree[indicesArray[temp++]!!]!!
        }

    }

    private var length = 0
    private var tree : Array<TaskMapEntry<K, V>?> = arrayOfNulls(1)

    private fun extendArray(){
        tree = tree.copyOf((tree.size shl 1) + 1)
    }

    override val size: Int
        get() = length

    override fun get(key: K): V? {
        val t = findPosition(key)
        return tree[t]?.value
    }

    private fun findPosition(key : K, position : Int = 0) : Int{
        while(tree.size-1 < position)
            extendArray()

        when {
            tree[position] == null -> return position
            tree[position]!!.key < key -> return findPosition(key, position * 2 + 2)
            tree[position]!!.key > key -> return findPosition(key, position * 2 + 1)
        }

        return position
    }

    operator fun set(key : K, value : V){
        val t = findPosition(key)
        if(tree[t] == null){
            length++
            tree[t] = TaskMapEntry(key, value)
        } else throw Exception()
    }

    override fun isEmpty(): Boolean {
        return length > 0
    }

    override fun containsKey(key: K): Boolean {
        return tree[findPosition(key)] != null
    }

    override fun containsValue(value: V): Boolean {
        for(i in tree)
            if (i?.value == value) return true
        return false
    }

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get(){
            val ans = TreeSet<MutableMap.MutableEntry<K, V>>()
            for(i in tree){
                ans += i ?: continue
            }
            return ans
        }

    override val keys: MutableSet<K>
        get(){
            val ans = TreeSet<K>()
            for(i in tree)
                ans += i?.key ?: continue
            return ans
        }

    override val values: MutableCollection<V>
        get(){
            val ans = TreeSet<V>()
            for(i in tree)
                ans += i?.value ?: continue
            return ans
        }

    override fun clear() {
        length = 0
        tree = arrayOfNulls(1)
    }

    override fun put(key: K, value: V): V? {
        set(key, value)
        return tree[findPosition(key)]!!.value
    }

    override fun putAll(from: Map<out K, V>) {
        for (i in from){
            put(i.key, i.value)
        }
    }

    override fun remove(key: K): V? {
        val pos = findPosition(key)
        return if (tree[pos] == null) null
        else{
            val ans = tree[pos]!!.value
            delete(pos)
            length--
            ans
        }
    }

    private fun delete(pos : Int){
        var replacement = findLargest(pos)
        if (replacement == pos) {
            replacement = findSmallest(pos)
        }
        tree[pos] = null
        if (replacement != pos) {
            val temp = tree[replacement]
            tree[replacement] = tree[pos]
            tree[pos] = temp
            delete(replacement)
        }
    }

    private fun findLargest(pos : Int) : Int{
        if(pos*2+2 < tree.size-1)
            if(tree[pos*2+2]!=null)
                return findLargest(pos*2+2)
        return pos
    }

    private fun findSmallest(pos : Int) : Int{
        if(pos*2+1 < tree.size-1)
            if(tree[pos*2+1]!=null)
                return findSmallest(pos*2+1)
        return pos
    }

    operator fun iterator(): TaskCollectionIterator {
        return TaskCollectionIterator()
    }

    operator fun set(any: Any?, value: V) {
        set(any as K, value)
    }
}