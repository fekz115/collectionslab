package org.fekz115.collectionslab.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.adapter.TableFieldsCreateAdapter
import org.fekz115.collectionslab.dao.Factories
import org.fekz115.collectionslab.dao.TableAlreadyExistException
import org.fekz115.collectionslab.model.DataObjectType

class TableCreateActivity : AppCompatActivity()  {

    private lateinit var tableSaveButton : Button
    private lateinit var fieldsRecyclerView: RecyclerView
    private lateinit var tableNameEditText: EditText
    private lateinit var addFieldButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.table_create_activity)
        initViews()

        val dataSource = intent.getSerializableExtra("dao") as Factories
        val factory = dataSource.getFactory(this)

        val tableCreateAdapter = TableFieldsCreateAdapter()
        fieldsRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@TableCreateActivity)
            adapter = tableCreateAdapter
        }

        addFieldButton.setOnClickListener {
            tableCreateAdapter.addField()
        }

        tableSaveButton.setOnClickListener {
            val fields = tableCreateAdapter.getFields()
            val tableName = tableNameEditText.text.toString()
            val dataTypeObject = DataObjectType(fields, tableName)
            try{
                factory.createTable(dataTypeObject)
            } catch (e : TableAlreadyExistException){
                Toast.makeText(this, resources.getText(R.string.table_exists_error), Toast.LENGTH_SHORT).show()
            }
            finish()
        }
    }

    private fun initViews(){
        tableSaveButton = findViewById(R.id.tableSave_button)
        tableNameEditText = findViewById(R.id.tableName_editText)
        fieldsRecyclerView = findViewById(R.id.fields_rv)
        addFieldButton = findViewById(R.id.addField_button)
    }

}