package org.fekz115.collectionslab.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView

class TableFieldsAdapter(private val fields : Array<Pair<String, Any>>) : RecyclerView.Adapter<TableFieldsAdapter.FieldViewHolder>(){

    class FieldViewHolder(itemView: TextView) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FieldViewHolder {
        return FieldViewHolder(TextView(p0.context))
    }

    override fun getItemCount(): Int {
        return fields.size
    }

    override fun onBindViewHolder(p0: FieldViewHolder, p1: Int) {
        (p0.itemView as TextView).text = "${fields[p1].first} : ${fields[p1].second}"
    }
}