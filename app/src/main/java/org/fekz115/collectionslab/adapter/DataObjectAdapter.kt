package org.fekz115.collectionslab.adapter

import android.app.AlertDialog
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.activity.ObjectCreateActivity
import org.fekz115.collectionslab.dao.DAO
import org.fekz115.collectionslab.dao.Factories
import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.DataObjectType

class DataObjectAdapter(private val dao : DAO, private val dataObjectType : DataObjectType, private val factory: Factories, private val tableNum : Int) : RecyclerView.Adapter<DataObjectAdapter.DataObjectViewHolder>() {

    private var sourceInTaskCollection = dao.read()
    private lateinit var source : Array<DataObject?>

    init {
        convertToArray()
    }

    private fun convertToArray(){
        var i = 0
        source = arrayOfNulls(sourceInTaskCollection.size)
        for(dataObject in sourceInTaskCollection){
            source[i++] = dataObject.value!!
        }
    }

    class DataObjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item = itemView
        val fieldsRecyclerView = itemView.findViewById<RecyclerView>(R.id.fields_rv_item)!!
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DataObjectViewHolder {
        return DataObjectViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.object_item, p0, false))
    }

    override fun getItemCount(): Int {
        return source.size
    }

    override fun onBindViewHolder(p0: DataObjectViewHolder, p1: Int) {
        p0.apply{
            item.setOnClickListener {
                val position = adapterPosition
                val selected = source[position]!!
                val builder = AlertDialog.Builder(it.context)
                builder.setTitle(dataObjectType.name)
                builder.setItems(R.array.object_dialog_items) { dialog, which ->
                    when(it.context.resources.getStringArray(R.array.object_dialog_items)[which]){
                        it.context.resources.getString(R.string.edit) -> {
                            val intent = Intent(it.context, ObjectCreateActivity::class.java)
                            intent.putExtra("type", tableNum)
                            intent.putExtra("dao", factory)
                            intent.putExtra("position", adapterPosition)
                            startActivity(it.context, intent, null)
                        }
                        it.context.resources.getString(R.string.delete) -> {
                            dao.delete(selected)
                            sourceInTaskCollection = dao.read()
                            convertToArray()
                            notifyItemRemoved(position)
                        }
                    }
                }
                builder.show()    
            }
            fieldsRecyclerView.apply{
                val array = Array(dataObjectType.fields.size){i -> Pair(dataObjectType.fields[i].first, source[p1]!!.values[i]!!)}
                layoutManager = LinearLayoutManager(this.context)
                adapter = TableFieldsAdapter(array)
            }
            item.isClickable = true
        }
    }

    fun updateItems(){
        sourceInTaskCollection = dao.read()
        convertToArray()
        notifyDataSetChanged()
    }
}