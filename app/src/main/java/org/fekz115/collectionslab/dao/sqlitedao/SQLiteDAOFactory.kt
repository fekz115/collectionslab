package org.fekz115.collectionslab.dao.sqlitedao

import android.content.Context
import org.fekz115.collectionslab.dao.DAO
import org.fekz115.collectionslab.dao.DAOFactory
import org.fekz115.collectionslab.dao.TableAlreadyExistException
import org.fekz115.collectionslab.model.DataObjectType
import org.fekz115.collectionslab.model.DataTypes

class SQLiteDAOFactory(context : Context) : DAOFactory(context) {

    private val dbHelper = FeedReaderDbHelper(context)
    private val db = dbHelper.writableDatabase

    init {
        if(!checkIfTableExist("Tables")){
            db.execSQL("CREATE TABLE Tables (`Id` INT PRIMARY KEY, `Name` TEXT UNIQUE NOT NULL);")
        }
        /*if(!checkIfTableExist("TablesFields")){
            db.execSQL("CREATE TABLE TablesFields (`TableId` INTEGER REFERENCES Tables (`Id`) ON DELETE CASCADE ON UPDATE CASCADE, `FieldOrder` INTEGER NOT NULL, `FieldName` TEXT NOT NULL, FieldType TEXT, PRIMARY KEY (`TableId`, `FieldOrder`));")
        }*/
    }

    private fun SQL_CREATE_TABLE(dataObjectType: DataObjectType) : String {
        return "CREATE TABLE `${dataObjectType.name}` (`${dataObjectType.fields[0].first}` ${SQLiteDAO.FieldConverter().convertFieldToDbType(dataObjectType.fields[0].second)} PRIMARY KEY ${if(SQLiteDAO.FieldConverter().getFieldsColumns(dataObjectType).isNotEmpty()){","}else{""}} ${SQLiteDAO.FieldConverter().getFieldsColumns(dataObjectType)});"
    }

    override fun getTables(): Array<Pair<DataObjectType, DAO>> {
        var j = 0
        val tablesCursor = db.rawQuery("SELECT * FROM `Tables`;", null)
        val tablesArray = arrayOfNulls<Pair<DataObjectType, DAO>>(tablesCursor.count)
        while(tablesCursor.moveToNext()){
            val tableId = tablesCursor.getInt(0)
            val tableName = tablesCursor.getString(1)
            val tableFieldsCursor = db.rawQuery("PRAGMA table_info(`$tableName`);", null)
            tableFieldsCursor.moveToFirst()
            val fieldsArray = Array<Pair<String, DataTypes>>(tableFieldsCursor.count) {
                println(tableFieldsCursor.getString(2))
                val dataType = when(tableFieldsCursor.getString(2)){
                    "REAL" -> DataTypes.REAL
                    "INTEGER" -> DataTypes.INT
                    else -> DataTypes.STRING
                }
                val temp = tableFieldsCursor.getString(1)
                tableFieldsCursor.moveToNext()
                Pair(temp, dataType)
            }
            tableFieldsCursor.close()
            val dataObjectType = DataObjectType(fieldsArray, tableName)
            tablesArray[j++] = Pair(dataObjectType, SQLiteDAO(dataObjectType, db))
        }
        tablesCursor.close()
        return tablesArray as Array<Pair<DataObjectType, DAO>>
    }

    override fun createTable(table: DataObjectType) : DAO{
        if(checkIfTableExist(table.name)) throw TableAlreadyExistException()
        db.execSQL(SQL_CREATE_TABLE(table))
        db.execSQL("INSERT INTO `Tables` (`name`) VALUES ('${table.name}');")
        return SQLiteDAO(table, db)
    }

    override fun deleteTable(table: DataObjectType) {
        db.execSQL("DROP TABLE `${table.name}`;")
        db.execSQL("DELETE FROM `Tables` WHERE `name` = '${table.name}';")
    }

    private fun checkIfTableExist(name : String) : Boolean {
        val cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='$name';", null)
        return cursor.count != 0
    }

    override fun onExit() {
        db.close()
    }

}