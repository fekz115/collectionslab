package org.fekz115.collectionslab.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.adapter.TableAdapter
import org.fekz115.collectionslab.dao.DAOFactory
import org.fekz115.collectionslab.dao.Factories
import org.fekz115.collectionslab.dao.sqlitedao.SQLiteDAOFactory

class MainActivity : AppCompatActivity() {

    private lateinit var tableRecyclerView : RecyclerView
    private lateinit var createTableFab : FloatingActionButton
    private lateinit var pullToRefresh : SwipeRefreshLayout

    private var usedDataSource = Factories.sqlite
    private lateinit var factory : DAOFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.table_activity)
        initViews()
        title = getString(R.string.tables)
        factory = SQLiteDAOFactory(this)
        createTableFab.setOnClickListener {
            val intent = Intent(this, TableCreateActivity::class.java)
            intent.putExtra("dao", usedDataSource)
            startActivity(intent)
        }
        tableRecyclerView.layoutManager = LinearLayoutManager(this)
        refresh()
        pullToRefresh.setOnRefreshListener{refresh(); pullToRefresh.isRefreshing = false}
    }

    private fun refresh(){
        val tables = factory.getTables()
        tableRecyclerView.adapter = TableAdapter(tables, usedDataSource, factory)
    }

    private fun initViews(){
        tableRecyclerView = findViewById(R.id.tables_rv)
        pullToRefresh = findViewById(R.id.pullToRefresh)
        createTableFab = findViewById(R.id.createTable_fab)
    }

}
