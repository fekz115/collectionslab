package org.fekz115.collectionslab.activity;

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.Toast
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.adapter.ObjectValuesAdapter
import org.fekz115.collectionslab.dao.DAO
import org.fekz115.collectionslab.dao.Factories
import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.DataObjectType
import java.sql.SQLException

class ObjectCreateActivity: AppCompatActivity() {

    private lateinit var fieldsRecyclerView: RecyclerView
    private lateinit var saveButton : Button
    private lateinit var dataType : DataObjectType
    private lateinit var dao : DAO
    private var position : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.object_create_activity)
        initViews()
        initVars()

        val objectValuesAdapter = if(position != -1){
            val t = dao.read()
            var i = 0
            var b : DataObject? = null
            for(a in t){
                if(i == position)b = a.value
                i++
            }
            ObjectValuesAdapter(b!!)
        } else {
            ObjectValuesAdapter(DataObject(dataType))
        }

        fieldsRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@ObjectCreateActivity)
            adapter = objectValuesAdapter
        }

        saveButton.setOnClickListener {
            if(position == -1) {
                try{
                    dao.create(objectValuesAdapter.dataObject)
                    finish()
                } catch (e : SQLiteConstraintException) {
                    Toast.makeText(this, getString(R.string.error_with_the_same_key), Toast.LENGTH_SHORT).show()
                }
                catch (e : Exception){
                    e.printStackTrace()
                }
            } else {
                dao.update(objectValuesAdapter.dataObject)
                finish()
            }
        }

        title = dataType.name
    }

    private fun initViews(){
        fieldsRecyclerView = findViewById(R.id.values_rv)
        saveButton = findViewById(R.id.objectSave_button)
    }

    private fun initVars(){
        val factoryType = intent.getSerializableExtra("dao") as Factories
        val p1 = intent.getIntExtra("type", -1)
        position = intent.getIntExtra("position", -1)
        val t = factoryType.getFactory(this).getTables()[p1]
        dataType = t.first
        dao = t.second
    }
}
