package org.fekz115.collectionslab.model

import java.io.Serializable

data class DataObjectType(val fields : Array<Pair<String, DataTypes>>, val name : String) : Serializable {

    fun checkObject(dataObject: DataObject) : Boolean {
        return dataObject.dataObjectType == this
    }

    fun getColumns() : List<String> {
        return fields.map { it.first }
    }

    fun getColumnTypes() : List<DataTypes> {
        return fields.map { it.second }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DataObjectType

        if (!fields.contentEquals(other.fields)) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = fields.contentHashCode()
        result = 31 * result + name.hashCode()
        return result
    }
}

enum class DataTypes{
    INT, STRING, REAL;
}

class DataObject(val dataObjectType: DataObjectType) : Serializable {

    val values : Array<Any?> = arrayOfNulls(dataObjectType.fields.size)

    fun get(s : String) : Any? {
        val t = dataObjectType.fields.indexOfFirst { it.first == s }
        return if(t != -1) values[t] else null
    }

    operator fun set(s: String, value: Any) {
        val t = dataObjectType.fields.indexOfFirst { it.first == s }
        if(t != -1) values[t] = value
    }

    override fun toString(): String {
        val ans = StringBuilder(dataObjectType.name + " {\n")
        for((i, col) in dataObjectType.getColumns().withIndex()){
            ans.append("\t" + col + " = " + values[i] + "\n")
        }
        ans.append("}")
        return ans.toString()
    }

}