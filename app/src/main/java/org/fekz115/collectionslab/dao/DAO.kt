package org.fekz115.collectionslab.dao

import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.TaskCollection
import java.io.Serializable

interface DAO : Serializable {
    fun create(t : DataObject) : Boolean
    fun read()                 : TaskCollection<Comparable<Any>, DataObject?>
    fun update(t : DataObject) : Boolean
    fun delete(t : DataObject) : Boolean
}