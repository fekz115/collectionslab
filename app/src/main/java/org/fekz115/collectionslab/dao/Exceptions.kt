package org.fekz115.collectionslab.dao

class TableAlreadyExistException(message : String = "Table with the same name already exist") : Exception(message)
