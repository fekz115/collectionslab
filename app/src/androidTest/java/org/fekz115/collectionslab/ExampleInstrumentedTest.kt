package org.fekz115.collectionslab

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.fekz115.collectionslab.dao.sqlitedao.SQLiteDAO
import org.fekz115.collectionslab.dao.sqlitedao.SQLiteDAOFactory
import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.DataObjectType
import org.fekz115.collectionslab.model.DataTypes

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val factory = SQLiteDAOFactory(appContext)
        printTables(factory)
        val fields = arrayOf(Pair("id", DataTypes.INT), Pair("name", DataTypes.STRING), Pair("value", DataTypes.REAL))
        factory.createTable(DataObjectType(fields, "TestTable"))
        printTables(factory)
    }

    fun printTables(factory : SQLiteDAOFactory){
        val tables = factory.getTables()
        for(i in tables){
            println(i)
        }
    }
}
