package org.fekz115.collectionslab.adapter

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.DataTypes

class ObjectValuesAdapter(val dataObject: DataObject) : RecyclerView.Adapter<ObjectValuesAdapter.FieldsViewHolder>() {

    class FieldsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FieldsViewHolder {
        val text = EditText(p0.context)
        text.width = text.maxWidth
        return FieldsViewHolder(text)
    }

    override fun getItemCount(): Int {
        return dataObject.dataObjectType.fields.size
    }

    override fun onBindViewHolder(p0: FieldsViewHolder, p1: Int) {
        val t = p0.itemView as EditText
        if((dataObject.values[0] != null) and (p1 == 0)){
            t.isEnabled = false
        }
        t.setText(dataObject.values[p1]?.toString())
        t.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {dataObject.values[p0.adapterPosition] = t.text}
        })
        t.apply {
            hint = dataObject.dataObjectType.fields[p1].first
            if ((dataObject.dataObjectType.fields[p1].second == DataTypes.INT) or (dataObject.dataObjectType.fields[p1].second == DataTypes.REAL)) {
                inputType = InputType.TYPE_CLASS_NUMBER
            }
            inputType = when(dataObject.dataObjectType.fields[p1].second){
                DataTypes.INT -> InputType.TYPE_CLASS_NUMBER
                DataTypes.REAL -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                else -> inputType
            }
        }
    }


}