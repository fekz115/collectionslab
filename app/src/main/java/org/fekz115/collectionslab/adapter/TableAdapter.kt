package org.fekz115.collectionslab.adapter

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.activity.MainActivity
import org.fekz115.collectionslab.activity.ObjectsActivity
import org.fekz115.collectionslab.dao.DAO
import org.fekz115.collectionslab.dao.DAOFactory
import org.fekz115.collectionslab.dao.Factories
import org.fekz115.collectionslab.model.DataObjectType

class TableAdapter(private var source : Array<Pair<DataObjectType, DAO>>, private val usedDataSource : Factories, private val daoFactory: DAOFactory) : RecyclerView.Adapter<TableAdapter.TableViewHolder>() {

    class TableViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item = itemView
        val tableName = itemView.findViewById<TextView>(R.id.tableName_item)!!
        val tableFieldsRecyclerView = itemView.findViewById<RecyclerView>(R.id.table_fields_rv_item)!!
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TableViewHolder {
        return TableViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.table_item, p0, false))
    }

    override fun getItemCount(): Int {
        return source.size
    }

    override fun onBindViewHolder(p0: TableViewHolder, p1: Int) {
        p0.apply {
            tableName.text = source[p1].first.name
            tableFieldsRecyclerView.apply {
                adapter = TableFieldsAdapter(source[p1].first.fields.map{ Pair(it.first, it.second as Any) }.toTypedArray())
                layoutManager = LinearLayoutManager(item.context)
            }
            item.setOnClickListener {
                val position = adapterPosition
                val builder = AlertDialog.Builder(it.context)
                builder.setTitle(R.string.table_dialog_title)
                builder.setItems(R.array.table_dialog_items) { dialog, which ->
                    when(item.context.resources.getStringArray(R.array.table_dialog_items)[which]){
                        item.context.resources.getString(R.string.open) -> {
                            val intent = Intent(item.context, ObjectsActivity::class.java)
                            intent.putExtra("dao", usedDataSource)
                            intent.putExtra("type", position)
                            item.context.startActivity(intent)
                        }
                        item.context.resources.getString(R.string.delete) -> {
                            daoFactory.deleteTable(source[position].first)
                            source = daoFactory.getTables()
                            notifyItemRemoved(position)
                        }
                    }
                }
                builder.show()
            }
            item.isClickable = true
        }
    }
}