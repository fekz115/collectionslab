package org.fekz115.collectionslab.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.adapter.DataObjectAdapter
import org.fekz115.collectionslab.dao.DAO
import org.fekz115.collectionslab.dao.Factories
import org.fekz115.collectionslab.model.DataObjectType

class ObjectsActivity : AppCompatActivity() {

    private lateinit var objectRecyclerView : RecyclerView
    private lateinit var dataObjectType : DataObjectType
    private lateinit var dao : DAO
    private lateinit var pullToRefresh : SwipeRefreshLayout
    private lateinit var fabCreateObject : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.object_activity)
        initViews()

        val p1 = intent.getIntExtra("type", 0)
        val usedSource = intent.getSerializableExtra("dao") as Factories
        val factory = usedSource.getFactory(this)
        val tables = factory.getTables()
        dao = tables[p1].second
        dataObjectType = tables[p1].first
        title = dataObjectType.name

        objectRecyclerView.layoutManager = LinearLayoutManager(this)
        objectRecyclerView.adapter = DataObjectAdapter(dao, dataObjectType, usedSource, p1)

        pullToRefresh.setOnRefreshListener {
            (objectRecyclerView.adapter as DataObjectAdapter).updateItems()
            pullToRefresh.isRefreshing = false
        }

        fabCreateObject.setOnClickListener {
            val intent = Intent(this, ObjectCreateActivity::class.java)
            intent.putExtra("type", p1)
            intent.putExtra("dao", usedSource)
            startActivity(intent)
        }

    }

    private fun initViews(){
        objectRecyclerView = findViewById(R.id.objects_rv)
        pullToRefresh = findViewById(R.id.pullToRefreshObjects)
        fabCreateObject = findViewById(R.id.add_object_button)
    }
}