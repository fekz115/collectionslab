package org.fekz115.collectionslab.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import org.fekz115.collectionslab.R
import org.fekz115.collectionslab.model.DataTypes

class TableFieldsCreateAdapter  : RecyclerView.Adapter<TableFieldsCreateAdapter.FieldEditViewHolder>(){

    private val tableFieldsViews = ArrayList<Pair<EditText, Spinner>>()
    private var fieldsCount = 1


    class FieldEditViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val fieldName = itemView.findViewById<EditText>(R.id.fieldName_editText)!!
        val fieldType = itemView.findViewById<Spinner>(R.id.fieldType_spinner)!!
        val deleteField = itemView.findViewById<Button>(R.id.deleteField_button)!!
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FieldEditViewHolder {
        return FieldEditViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.table_field_edit_item, p0, false))
    }

    override fun getItemCount(): Int {
        return fieldsCount
    }

    override fun onBindViewHolder(p0: FieldEditViewHolder, p1: Int) {
        p0.apply {
            tableFieldsViews.add(Pair(fieldName, fieldType))
            fieldType.adapter = ArrayAdapter(fieldType.context, android.R.layout.simple_dropdown_item_1line, DataTypes.values())
            if(p1 == 0)
                deleteField.visibility = View.GONE
            deleteField.setOnClickListener{
                fieldsCount--
                tableFieldsViews.removeAt(p1)
                this@TableFieldsCreateAdapter.notifyItemRemoved(p1)
            }
        }
    }

    fun getFields() :  Array<Pair<String, DataTypes>>{
        return tableFieldsViews.stream().map{
            Pair(it.first.text.toString(), DataTypes.valueOf(it.second.selectedItem.toString()))
        }.toArray<Pair<String, DataTypes>>{length -> arrayOfNulls(length)}
    }

    fun addField(){
        fieldsCount++
        notifyItemInserted(itemCount)
    }

}