package org.fekz115.collectionslab

import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.DataObjectType
import org.fekz115.collectionslab.model.DataTypes
import org.junit.Test

class DataObjectTest{
    @Test
    fun test(){
        val fields = arrayOf(Pair("Id", DataTypes.INT), Pair("Name", DataTypes.STRING))
        val personType = DataObjectType(fields, "Person")
        val person = DataObject(personType)
        person["Id"] = 5
        person["Name"] = "John"
        println(person)
        val person2 = DataObject(personType)
        person2["Id"] = 1
        person2["Name"] = "Nick"
        println(person2)

    }
}