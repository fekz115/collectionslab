package org.fekz115.collectionslab.dao.sqlitedao

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import org.fekz115.collectionslab.dao.DAO
import org.fekz115.collectionslab.model.DataObject
import org.fekz115.collectionslab.model.DataObjectType
import org.fekz115.collectionslab.model.DataTypes
import org.fekz115.collectionslab.model.TaskCollection

class SQLiteDAO(private val dataObjectType : DataObjectType, private val db : SQLiteDatabase) : DAO {

    class FieldConverter{
        fun convertFieldToDbType(type : DataTypes) : String {
            return when(type) {
                DataTypes.INT -> "INTEGER"
                DataTypes.STRING -> "TEXT"
                DataTypes.REAL -> "REAL"
            }
        }

        fun convertValueToDbType(type : DataTypes, value : Any?): String {
            return when(type) {
                DataTypes.INT -> "'$value'"
                DataTypes.STRING -> "'$value'"
                DataTypes.REAL -> "'$value'"
            }
        }

        fun convertValues(dataObject: DataObject) : String{
            val values = arrayOfNulls<Any?>(dataObject.values.size)
            for(i in dataObject.values.indices){
                values[i] = convertValueToDbType(dataObject.dataObjectType.fields[i].second, dataObject.values[i])
            }
            return "(${values.joinToString(", ")})"
        }

        fun convertFieldsToPair(dataObject: DataObject) : String {
            val values = arrayOfNulls<Any?>(dataObject.values.size)
            for(i in dataObject.values.indices){
                values[i] = "`${dataObject.dataObjectType.fields[i].first}` = ${convertValueToDbType(dataObject.dataObjectType.fields[i].second, dataObject.values[i])}"
            }
            return values.joinToString(", ")
        }

        fun getFieldsColumns(dataObjectType: DataObjectType) : String {
            val values = arrayOfNulls<Any?>(dataObjectType.fields.size-1)
            for(i in 1..values.size){
                values[i-1] = "`${dataObjectType.fields[i].first}` ${convertFieldToDbType(dataObjectType.fields[i].second)}"
            }
            return values.joinToString(", ")
        }

        fun convertAnswerToValue(dataType: DataTypes, cursor: Cursor, i : Int) : Any{
            return when(dataType){
                DataTypes.INT -> cursor.getInt(i)
                DataTypes.STRING -> cursor.getString(i)
                DataTypes.REAL -> cursor.getFloat(i)
            }
        }

    }

    private fun SQL_SELECT_ALL()                   : String {return "SELECT * FROM `${dataObjectType.name}`;"}
    private fun SQL_INSERT(dataObject: DataObject) : String {return "INSERT INTO `${dataObjectType.name}` (${dataObject.dataObjectType.fields.joinToString(", ") {"`${it.first}`"}}) VALUES ${FieldConverter().convertValues(dataObject)};"}
    private fun SQL_DELETE(dataObject: DataObject) : String {return "DELETE FROM `${dataObjectType.name}` WHERE ${dataObjectType.fields[0].first} = ${FieldConverter().convertValueToDbType(dataObjectType.fields[0].second, dataObject.values[0])};"}
    private fun SQL_UPDATE(dataObject: DataObject) : String {return "UPDATE `${dataObjectType.name}` SET ${FieldConverter().convertFieldsToPair(dataObject)} WHERE ${dataObjectType.fields[0].first} = ${FieldConverter().convertValueToDbType(dataObjectType.fields[0].second, dataObject.values[0])};"}

    override fun create(t: DataObject): Boolean {
        println(SQL_INSERT(t))
        db.execSQL(SQL_INSERT(t))
        return true
    }

    override fun read(): TaskCollection<Comparable<Any>, DataObject?> {
        println(SQL_SELECT_ALL())
        val cursor = db.rawQuery(SQL_SELECT_ALL(), null)
        val ans = TaskCollection<Comparable<Any>, DataObject?>()
        cursor.moveToFirst()
        if(cursor.count != 0)
        do{
            val dataObject = DataObject(dataObjectType)
            for(j in dataObject.values.indices){
                dataObject.values[j] = FieldConverter().convertAnswerToValue(dataObjectType.fields[j].second, cursor, j)
            }
            ans[dataObject.values[0]] = dataObject
        }while(cursor.moveToNext())
        return ans
    }

    override fun update(t: DataObject): Boolean {
        println(SQL_UPDATE(t))
        db.execSQL(SQL_UPDATE(t))
        return true
    }

    override fun delete(t: DataObject): Boolean {
        println(SQL_DELETE(t))
        db.execSQL(SQL_DELETE(t))
        return true
    }
}